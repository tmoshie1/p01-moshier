//
//  AppDelegate.h
//  Hello World
//
//  Created by Tom Moshier on 1/23/17.
//  Copyright © 2017 Tom Moshier. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

