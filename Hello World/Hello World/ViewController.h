//
//  ViewController.h
//  Hello World
//
//  Created by Tom Moshier on 1/23/17.
//  Copyright © 2017 Tom Moshier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *message;

@end

