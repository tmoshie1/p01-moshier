//
//  main.m
//  Hello World
//
//  Created by Tom Moshier on 1/23/17.
//  Copyright © 2017 Tom Moshier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
